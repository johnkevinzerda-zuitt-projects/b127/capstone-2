const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({

	email: {
		type: String,
		require: [true, 'email is required']
	},
	password: {
		type: String,
		require: [true, 'password is required']
	},
	lastName: {
		type: String,
		require: [true, 'Last name is required']
	},
	firstName: {
		type: String,
		require: [true, 'First name is required']
	},
	mobileNumber: {
		type: String,
		require: [true, 'Mobile number is required']
	},
	isAdmin: {
		type: Boolean,
		default: false
	}
	
})

module.exports = mongoose.model("User", userSchema)