const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({

	name: {
		type: String,
		require: [true, 'name is required']
	},
	description: {
		type: String,
		require: [true, 'description is required']
	},
	price: {
		type: Number,
		require: [true, 'price is required']
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	}

})

module.exports = mongoose.model("Product", productSchema)