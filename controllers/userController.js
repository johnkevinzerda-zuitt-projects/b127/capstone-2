const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');

module.exports.register = (data) => {
	return User.findOne({email: data.email}).then(result => {

		let newUser = new User({
			email: data.email,
			password: bcrypt.hashSync(data.password, 0),
			lastName: data.lastName,
			firstName: data.firstName,
			mobileNumber: data.mobileNumber,
			isAdmin: data.isAdmin
		})
		return newUser.save().then((user,isError) => { 
			return (isError)? false : true	
		})
	})
}

module.exports.checkEmailExist = (reqBody) => {
	return User.find({email:reqBody.email}).then(result=> {
		if (result.length >0) {
			return true;
		} else {
			return false;
		}
	})
}

module.exports.makeUserAdmin = (reqBody) => {
	let makeAdmin = {
		isAdmin: reqBody.isAdmin
	}
	return User.findByIdAndUpdate(reqBody.userId,makeAdmin).then((update,isError) => {
		return (isError)? false : true;
	})
}



module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if (isPasswordCorrect) {
				return {accessToken: auth.createAccessToken(result.toObject())}
			} else {
				return false;
			}
		}
	})
}

module.exports.userProfile = (data) => {
	return User.findById( data.userId ).then(result => {
		return result;
	})
}


