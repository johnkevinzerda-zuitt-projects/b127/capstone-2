const Product = require('../models/Product');


module.exports.getAllProducts = () => {
	return Product.find().then(result => {
		return result;
	})
}


module.exports.getSpecificProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}


module.exports.createProduct = (data) => {

	return Product.findOne({name: data.product.name}).then(result => {
		if (result !== null && result.name == data.product.name) {
			return false;
		}else {
			let newProduct = new Product({
				name: data.product.name,
				description: data.product.description,
				price: data.product.price
			})
			return newProduct.save().then((product,isError) => {
				return (isError)? false : true;
			})
		}
	})
}
	
	

module.exports.modifyProduct = (reqParams,data) => {

	let modifiedProduct = {
			name: data.newUpdate.name,
			description: data.newUpdate.description,
			price: data.newUpdate.price,
			isActive: data.newUpdate.isActive
	};
	return Product.findByIdAndUpdate(reqParams.productId, modifiedProduct).then((course,isError) => {
		return (isError)? false : true;
	})

}
