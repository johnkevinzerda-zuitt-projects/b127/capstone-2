const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController')
const auth = require('../auth');


router.post('/register', (req,res)=>{
	userController.register(req.body).then(result => res.send(result));
})

router.post('/register/admin', (req,res)=>{
	req.body.isAdmin = true;
	userController.register(req.body).then(result => res.send(result));
})

router.post('/checkEmail', (req, res) =>{
	userController.checkEmailExist(req.body).then(result => res.send(result));
})


router.put('/makeAdmin', auth.verify, (req,res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		userController.makeUserAdmin(req.body).then(result => res.send(result))
	}else {
		res.send(false)
	}
})



router.post('/login', (req,res) => {
	userController.loginUser(req.body).then(result => res.send(result))
})


router.get('/checkDetail', auth.verify, (req,res) => {
	 
	const userData =  auth.decode(req.headers.authorization)
	
	userController.userProfile({userId:userData.id}).then(result => res.send(result));
})



module.exports = router;