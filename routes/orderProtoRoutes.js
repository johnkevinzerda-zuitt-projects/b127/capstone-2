const express = require('express');
const router = express.Router();
const orderControllerProtoType = require('../controllers/orderControllerPrototype')
const auth = require('../auth');

router.post('/createOrderProto', auth.verify, (req,res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId: auth.decode(req.headers.authorization).id
	}

	if(!data.isAdmin){
		orderControllerProtoType.createOrderProto(data).then(result => res.send(result))
	}else {
		res.send(false)
	}

})

router.get('/allOrdersProto', auth.verify, (req,res) =>  {
	const data =  {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if (data.isAdmin) {
		orderControllerProtoType.getAllOrdersProto().then(result => res.send(result))
	}else {
		res.send(false);
	}
})


router.get('/myOrdersProto', (req,res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId: auth.decode(req.headers.authorization).id
	}
	console.log(data.userId)
	if (!data.isAdmin) {
		orderControllerProtoType.myOrdersProto(data).then(result => res.send(result))
	} else {
		res.send(false);
	}
})


module.exports = router;