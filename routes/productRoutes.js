const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController')
const auth = require('../auth');


router.get('/getAllProducts',(req,res) => {
	productController.getAllProducts().then(result=> res.send(result));
})



router.get('/:productId',(req,res) => {
	productController.getSpecificProduct(req.params).then(result => res.send(result));
})



router.post('/createProduct', auth.verify, (req,res) => {
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	
	if(data.isAdmin){
	productController.createProduct(data).then(result => res.send(result))
	}else {
		res.send(false)
	}

})



router.put('/modify/:productId/', auth.verify, (req,res) => {

	const data = {
		newUpdate: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		productController.modifyProduct(req.params, data).then(result => res.send(result))
	}else {
		res.send(false)
	}
})


router.put('/archive/:productId/', auth.verify, (req,res) => {

	const data = {
		newUpdate: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		data.newUpdate.isActive = false;
		productController.modifyProduct(req.params, data).then(result => res.send(result))
	}else {
		res.send(false)
	}
})


router.put('/unarchive/:productId/', auth.verify, (req,res) => {

	const data = {
		newUpdate: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
		data.newUpdate.isActive = true;
		productController.modifyProduct(req.params, data).then(result => res.send(result))
	}else {
		res.send(false)
	}
})




module.exports = router;